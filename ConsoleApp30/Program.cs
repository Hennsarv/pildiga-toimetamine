﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace ConsoleApp30
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "https://upload.wikimedia.org/wikipedia/commons/b/bd/Nile_crocodile_head.jpg";
            string url2 = "http://www.clipartbest.com/cliparts/RiG/y98/RiGy986iL.jpeg";
            string filename = @"..\..\loetudpilt.jpg";

            // hakatuseks loeme veebist pildi (kui on URL millel pilt avaneb)
            // WebRequest - see sõnum, mis läheb minult serverile - vaikimisi GET
            WebRequest request = WebRequest.Create(url2);
            
            // Response on siis vastus sellele, eeldame et HTTP annab vastuse HTTP
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Console.WriteLine(response.StatusDescription);
            var stream = response.GetResponseStream();

            // variant 1 - loeme vastuse baithaaval

            //List<byte> baidid = new List<byte>();
            //int bait = 0;
            //do
            //{
            //    bait = stream.ReadByte();
            //    if (bait != -1) baidid.Add((byte)bait);
            //} while (bait != -1);
            //File.WriteAllBytes(filename, baidid.ToArray());

            //variant 2 - salvestame vastuse faili
            //using (var filestream = File.Create(filename))
            //{
            //    stream.CopyTo(filestream);
            //}

            // variant 3 - salvestame vastuse läbi filestreami
            using (var memorystream = new MemoryStream())
            {
                stream.CopyTo(memorystream);
                byte[] baidid = memorystream.ToArray();
                NorthwindEntities ne = new NorthwindEntities();
                ne.Categories.Where(x => x.CategoryName == "Krokodillid").SingleOrDefault().Picture = baidid;
                ne.SaveChanges();
            }



        }
    }
}
